&7&lFrequently asked questions
&7Q: How do I get started on this server?
&bA&7: &rFirst teleport to our plot world by doing &b/plots&r. Then, you can claim a plot by doing &b/p auto&r and start building!

&7Q: How do I get promoted?
&bA&7: &rTo be promoted to &7Member&r, do &b/apply&r after you've met the prerequisites as a &8Guest&r. Beyond that, we ask that players don't bother us asking for ranks. We're constantly on the lookout and promote players when we feel it's appropriate.

&7Q: How do I donate?
&bA&7: &rDo /donate!

&7Q: Where is the server located?
&bA&7: &rGermany.

&7Q: Does Synergy have a Discord server or Dubtrack room?
&bA&7: &rWe do, and you can connect your account to those services through the &b/connection&r command.

&7Q: Does Synergy have a website?
&bA&7: &rOur website is currently a work in progress, but you can still check it out at &bhttps://synergyserver.net&r!

&7For more information, check out our website: &bhttps://synergyserver.net/help/faq