package net.synergyserver.synergycore.profiles;

import java.util.HashMap;
import java.util.UUID;

/**
 * Represents a <code>ProfileManager</code>, which handles the
 * caching and uncaching of certain <code>Profile</code>s.
 */
public class ProfileManager {

    private static ProfileManager instance;
    private HashMap<UUID, MinecraftProfile> cachedMinecraftProfiles;

    /**
     * Creates a new <code>ProfileManager</code> object.
     */
    private ProfileManager() {
        cachedMinecraftProfiles = new HashMap<>();
    }

    /**
     * Returns the object representing this <code>ProfileManager</code>.
     *
     * @return The object of this class.
     */
    public static ProfileManager getInstance() {
        if (instance == null) {
            instance = new ProfileManager();
        }
        return instance;
    }

    /**
     * Gets a cached <code>MinecraftProfile</code> by its ID. If no matching <code>MinecraftProfile</code>s
     * are found in the cache, then this returns the live document stored in the database.
     *
     * @param pID The ID of the <code>MinecraftProfile</code> to get.
     * @return The requested <code>MinecraftProfile</code>, if found, or null if no documents matched in the database.
     */
    public MinecraftProfile getCachedMinecraftProfile(UUID pID) {
        return cachedMinecraftProfiles.get(pID);
    }

    /**
     * Stores a <code>MinecraftProfile</code> in the cache.
     *
     * @param mcp The <code>MinecraftProfile</code> to cache.
     * @return True if the profile's ID was not already found in the cache.
     */
    public boolean cacheMinecraftProfile(MinecraftProfile mcp) {
        if (cachedMinecraftProfiles.containsKey(mcp.getID())) {
            return false;
        }
        cachedMinecraftProfiles.put(mcp.getID(), mcp);
        return true;
    }

    /**
     * Removes a <code>MinecraftProfile</code> from the cache.
     *
     * @param pID The player ID of the <code>MinecraftProfile</code> to remove from the cache.
     * @return True if the profile was successfully removed from the cache.
     */
    public boolean uncacheMinecraftProfile(UUID pID) {
        if (!cachedMinecraftProfiles.containsKey(pID)) {
            return false;
        }
        cachedMinecraftProfiles.remove(pID);
        return true;
    }

    /**
     * Returns the HashMap containing all currently cached <code>MinecraftProfile</code>s.
     *
     * @return The cached <code>MinecraftProfile</code>s.
     */
    public HashMap<UUID, MinecraftProfile> getCachedMinecraftProfiles() {
        return cachedMinecraftProfiles;
    }
}
