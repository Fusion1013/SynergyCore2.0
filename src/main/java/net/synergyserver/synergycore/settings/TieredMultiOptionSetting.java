package net.synergyserver.synergycore.settings;

import net.synergyserver.synergycore.profiles.MinecraftProfile;
import org.bukkit.permissions.Permissible;

/**
 * Represents a <code>MultiOptionSetting</code> that has tiers of options
 * where each tier is inclusive of the behaviors of the previous tier.
 */
public abstract class TieredMultiOptionSetting extends MultiOptionSetting {

    /**
     * Creates a new <code>TieredMultiOptionSetting</code> with the given parameters. This should only be used by subclasses
     * of this class, and only by fields marked as public, static, and final.
     *
     * @param id The ID of this setting, used to reference it in official documents.
     * @param category The category of this setting, used to group settings together by their features.
     * @param description The description of what this setting does.
     * @param permission The permission node required to use this setting.
     * @param defaultValue The ID of the default <code>SettingOption</code> of this setting.
     * @param defaultValueNoPermission The default value of this setting, if the user
     *                                 does not have permission to access this setting.
     * @param optionsArray An array of <code>SettingOption</code>s that are valid values for this setting.
     */
    public TieredMultiOptionSetting(String id, SettingCategory category, String description, String permission,
                                    String defaultValue, String defaultValueNoPermission, SettingOption[] optionsArray) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission, optionsArray);
    }

    public boolean isValueAtLeast(MinecraftProfile minecraftProfile, String targetValue) {
        return isValueAtLeast(minecraftProfile.getCurrentWorldGroupProfile().getSettingPreferences().getCurrentSettings(),
                minecraftProfile.getPlayer(), targetValue);
    }

    /**
     * Checks if the given player's value for this setting is equal to or greater than the targeted value.
     *
     * @param diffs The <code>SettingDiffs</code> to get the setting value for.
     * @param permissible The Permissible to test the permission of this <code>Setting</code> against.
     * @param targetValue The targeted value to compare against.
     * @return True if the player's value is at least the targeted value.
     */
    public boolean isValueAtLeast(SettingDiffs diffs, Permissible permissible, String targetValue) {
        String currentValue = getValue(diffs, permissible);
        int currentValueIndex = -1;
        int targetValueIndex = -1;

        // Search for the current and target indices in the options list
        int i = 0;
        for (Object value : this.getOptions().keySet()) {
            if (value.equals(currentValue)) {
                currentValueIndex = i;
            }
            if (value.equals(targetValue)) {
                targetValueIndex = i;
            }
            i++;
        }

        return currentValueIndex >= targetValueIndex;
    }

    public boolean isValueAtMost(MinecraftProfile minecraftProfile, String targetValue) {
        return isValueAtMost(minecraftProfile.getCurrentWorldGroupProfile().getSettingPreferences().getCurrentSettings(),
                minecraftProfile.getPlayer(), targetValue);
    }

    /**
     * Checks if the given player's value for this setting is equal to or less than the targeted value.
     *
     * @param diffs The <code>SettingDiffs</code> to get the setting value for.
     * @param permissible The Permissible to test the permission of this <code>Setting</code> against.
     * @param targetValue The targeted value to compare against.
     * @return True if the player's value is at most the targeted value.
     */
    public boolean isValueAtMost(SettingDiffs diffs, Permissible permissible, String targetValue) {
        String currentValue = getValue(diffs, permissible);
        int currentValueIndex = -1;
        int targetValueIndex = -1;

        // Search for the current and target indices in the options list
        int i = 0;
        for (Object value : this.getOptions().keySet()) {
            if (value.equals(currentValue)) {
                currentValueIndex = i;
            }
            if (value.equals(targetValue)) {
                targetValueIndex = i;
            }
            i++;
        }

        return currentValueIndex <= targetValueIndex;
    }
}
