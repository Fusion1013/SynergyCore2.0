package net.synergyserver.synergycore.exceptions;

import net.synergyserver.synergycore.ServiceType;

/**
 * Thrown when a bot is unable to connect to the corresponding service.
 */
public class ServiceOfflineException extends RuntimeException {

    private ServiceType serviceType;

    /**
     * Creates a new <code>ServiceOfflineException</code> with the given <code>ServiceType</code>.
     *
     * @param serviceType The service that caused the exception.
     */
    public ServiceOfflineException(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * Gets the <code>ServiceType</code> of the service that caused the exception.
     *
     * @return The service that caused the exception.
     */
    public ServiceType getServiceType() {
        return serviceType;
    }
}
