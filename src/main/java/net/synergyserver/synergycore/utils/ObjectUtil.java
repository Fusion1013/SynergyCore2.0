package net.synergyserver.synergycore.utils;

/**
 * Utility that assists with miscellaneous Java objects.
 */
public class ObjectUtil {

    /**
     * Compares two objects and returns the larger of the two. If they are equal then the first object is favored.
     *
     * @param o1 The first object to compare.
     * @param o2 The second object to compare.
     * @param <T> The type of the compared objects.
     * @return The larger object.
     */
    public static <T extends Comparable<T>> T max(T o1, T o2) {
        return o1.compareTo(o2) >= 0 ? o1 : o2;
    }

    /**
     * Compares two objects and returns the smaller of the two. If they are equal then the first object is favored.
     *
     * @param o1 The first object to compare.
     * @param o2 The second object to compare.
     * @param <T> The type of the compared objects.
     * @return The smaller object.
     */
    public static <T extends Comparable<T>> T min(T o1, T o2) {
        return o1.compareTo(o2) <= 0 ? o1 : o2;
    }

}
