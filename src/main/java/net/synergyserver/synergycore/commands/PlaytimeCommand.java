package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "playtime",
        aliases = {"pt", "gametime"},
        permission = "syn.playtime",
        usage = "/playtime <check|top>",
        description = "Main command for checking playtime."
)
public class PlaytimeCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }

}
