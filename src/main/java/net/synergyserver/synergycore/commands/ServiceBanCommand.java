package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "serviceban",
        aliases = "sban",
        permission = "syn.serviceban",
        usage = "/serviceban <discord|dubtrack>",
        description = "Main command for banning players from a service.",
        minArgs = 2,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE}
)

public class ServiceBanCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
