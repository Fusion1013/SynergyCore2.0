package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.mongodb.morphia.Datastore;

import java.util.List;
import java.util.UUID;

@CommandDeclaration(
        commandName = "foo",
        aliases = {"bar", "ping"},
        permission = "syn.test.foo",
        usage = "/test foo baz",
        description = "This does shit",
        executeIfInvalidSubCommand = true,
        parentCommandName = "test"
)
public class FooCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // Convert homes to warps
        Datastore ds = MongoDB.getInstance().getDatastore();
        List<WorldGroupProfile> wgps = ds.find(WorldGroupProfile.class).field("hs").exists()
                .retrievedFields(true, "pid", "n", "hs").asList();

        for (WorldGroupProfile wgp : wgps) {
            // Ignore Survival homes since Survival is resetting anyways
            if (wgp.getWorldGroupName().equalsIgnoreCase("Survival")) {
                continue;
            }

            UUID pID = wgp.getPlayerID();

            wgp.getHomes().forEach((name, location) -> {
                int trial = 0;
                // If the player already has a warp with the name then keep adding one to the name
                long matchedWarp = ds.find(Warp.class).field("o").equal(pID).field("n").equalIgnoreCase(name).count();
                while (matchedWarp > 0) {
                    trial++;
                    matchedWarp = ds.find(Warp.class).field("o").equal(pID).field("n").equalIgnoreCase(name + trial).count();
                }
                if (trial > 0) {
                    name = name + trial;
                }

                Warp warp = new Warp(name, pID, location.getLocation());
                DataManager.getInstance().saveDataEntity(warp);

                Bukkit.broadcastMessage("Migrated home " + name + " of " + PlayerUtil.getName(pID) + " in the " + wgp.getWorldGroupName() + " world group.");
            });
        }
        return true;
    }
}
