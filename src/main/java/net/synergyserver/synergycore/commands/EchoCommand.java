package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "echo",
        permission = "syn.echo",
        usage = "/echo [message]",
        description = "Echos back a message."
)
public class EchoCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // If they didn't supply a message then echo back the default one
        if (args.length == 0) {
            sender.sendMessage(Message.get("commands.echo.default"));
            return true;
        }

        String message = String.join(" ", args);
        // If they have perms to format the message
        if (sender.hasPermission("syn.echo.format")) {
            message = ChatColor.translateAlternateColorCodes('&', message);
        }

        // Echo back the message
        sender.sendMessage(message);
        return true;
    }
}
