package net.synergyserver.synergycore.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents the flags and values given by a player while entering a command.
 */
public class CommandFlags {

    private Map<String, String> flags;

    /**
     * Creates a new <code>CommandFlags</code> object with the parsed flags from an array of arguments.
     *
     * @param args An array of command arguments to parse the flags from.
     */
    public CommandFlags(String[] args) {
        Map<String, String> flags = new HashMap<>();

        // Parse flags and store them in the instance variable
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                String flag = args[i];
                String value = "";

                // If the following word is not another flag then it's a value
                if ((i + 1 < args.length) && !args[i + 1].startsWith("-")) {
                    value = args[i + 1];
                }

                flags.put(flag.toLowerCase(), value.toLowerCase());
            }
        }
        this.flags = flags;
    }

    /**
     * Gets the Map containing flags and their values.
     *
     * @return The Map containing flags and their values.
     */
    public Map<String, String> getFlags() {
        return flags;
    }

    /**
     * Checks if one of the given flags was entered by the command sender.
     *
     * @param synonymousFlags A list of synonymous flags to check the status of.
     * @return True if one of the synonymous flags was present.
     */
    public boolean hasFlag(String... synonymousFlags) {
        for (String flag : synonymousFlags) {
            if (flags.containsKey(flag)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the flag that the player actually used from the list of synonymous flags. If the player did not use any
     * of the flags then this returns null.
     *
     * @param synonymousFlags The flags to attempt to find.
     * @return The flag used if found, or null.
     */
    public String getFlag(String... synonymousFlags) {
        for (String flag : synonymousFlags) {
            if (flags.containsKey(flag)) {
                return flag;
            }
        }
        return null;
    }

    /**
     * Gets the first non-null value for the synonymous flags given, if it is found. Otherwise, returns null.
     *
     * @param synonymousFlags The names of the flags to get a value for.
     * @return The first non-null value if found, or null.
     */
    public String getFlagValue(String... synonymousFlags) {
        for (String flag : synonymousFlags) {
            if (flags.containsKey(flag)) {
                return flags.get(flag);
            }
        }
        return null;
    }

    /**
     * Finds flags and their values in an array of arguments and removes them.
     *
     * @param args The arguments to remove flags from.
     * @return An array of strings, containing everything that wasn't a flag.
     */
    public static String[] stripFlags(String[] args) {
        List<String> newArgs = new ArrayList<>();

        for (int i = 0; i < args.length; i++) {
            // Don't add it to the new args if it's a flag
            if (args[i].startsWith("-")) {
                continue;
            }
            // Don't add it to the new args if it's a value of a flag
            if ((i - 1 >= 0) && args[i - 1].startsWith("-")) {
                continue;
            }

            // Otherwise, add it
            newArgs.add(args[i]);
        }

        return newArgs.toArray(new String[newArgs.size()]);
    }
}
