package net.synergyserver.synergycore.listeners;

import io.sponges.dubtrack4j.event.framework.EventBus;
import io.sponges.dubtrack4j.event.user.UserChatEvent;
import io.sponges.dubtrack4j.event.user.UserJoinEvent;
import io.sponges.dubtrack4j.event.user.UserLeaveEvent;
import io.sponges.dubtrack4j.framework.Room;
import io.sponges.dubtrack4j.framework.User;
import io.sponges.dubtrack4j.util.Role;
import net.synergyserver.synergycore.ServiceToken;
import net.synergyserver.synergycore.ServiceType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.ServiceConnectEvent;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.profiles.DubtrackProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * Listens to events that occur in the Dubtrack room.
 */
public class DubtrackListener {

    private String roomName;
    private DataManager dm;
    private static DubtrackListener instance = null;

    /**
     * Creates a new <code>DubtrackListener</code> object.
     */
    private DubtrackListener() {
        roomName = SynergyCore.getPluginConfig().getString("dubtrack.room");
        dm = DataManager.getInstance();
    }

    /**
     * Returns the object representing this <code>DubtrackListener</code>.
     *
     * @return The object of this class.
     */
    public static DubtrackListener getInstance() {
        if (instance == null) {
            instance = new DubtrackListener();
        }
        return instance;
    }

    private Consumer<UserChatEvent> chatListener = new Consumer<UserChatEvent>() {
        @Override
        public void accept(UserChatEvent event) {
            io.sponges.dubtrack4j.framework.Message message = event.getMessage();
            Room room = message.getRoom();

            // Ignore the event if the message wasn't sent in Synergy's room
            if (!room.getId().equalsIgnoreCase(roomName)) {
                return;
            }

            User author = message.getUser();

            // Ignore the event if the Dubtrack account is already connected to a user
            if (dm.isDataEntityInDatabase(DubtrackProfile.class, author.getId())) {
                return;
            }

            String content = message.getContent();

            for (Player player : Bukkit.getOnlinePlayers()) {
                MinecraftProfile mcp = PlayerUtil.getProfile(player);
                SynUser synUser = dm.getDataEntity(SynUser.class, mcp.getSynID());
                ServiceToken token = synUser.getDubtrackToken();

                if (token != null && token.getToken().equals(content)) {
                    // If the token is expired then remove the token from the database
                    if (token.isExpired()) {
                        synUser.setDubtrackToken(null);
                        break;
                    }

                    // Send a success message and promote user.
                    try {
                        room.sendMessage(Message.format("service_connection.token.accepted", "Dubtrack"));
                        room.setRole(author, Role.RESIDENT_DJ);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Create their DiscordProfile and connect it
                    synUser.setDubtrackID(author.getId());

                    DubtrackProfile dubtrackProfile = new DubtrackProfile(
                            synUser.getID(),
                            author.getId(),
                            author.getUsername()
                    );
                    dm.saveDataEntity(dubtrackProfile);

                    // Emit an event
                    ServiceConnectEvent serviceConnectEvent = new ServiceConnectEvent(synUser, ServiceType.DUBTRACK, dubtrackProfile);
                    Bukkit.getPluginManager().callEvent(serviceConnectEvent);
                    break;
                }
            }
        }
    };

    private Consumer<UserJoinEvent> joinListener = new Consumer<UserJoinEvent>() {
        @Override
        public void accept(UserJoinEvent event) {
            User user = event.getUser();
            DubtrackProfile dubtrackProfile = dm.getDataEntity(DubtrackProfile.class, user.getId());

            if (dubtrackProfile == null) {
                return;
            }

            if (!dubtrackProfile.getCurrentName().equals(user.getUsername())) {
                dubtrackProfile.setCurrentName(user.getUsername());
                dubtrackProfile.addKnownName(user.getUsername());
            }

            dubtrackProfile.setLastLogIn(System.currentTimeMillis());
        }
    };

    private Consumer<UserLeaveEvent> leaveListener = new Consumer<UserLeaveEvent>() {
        @Override
        public void accept(UserLeaveEvent event) {
            User user = event.getUser();
            DubtrackProfile dubtrackProfile = dm.getDataEntity(DubtrackProfile.class, user.getId());

            if (dubtrackProfile == null) {
                return;
            }

            dubtrackProfile.setLastLogOut(System.currentTimeMillis());
        }
    };

    /**
     * Registers the event listeners for the Dubtrack bot.
     */
    public void registerListeners() {
        try {
            EventBus eventBus = SynergyCore.getDubtrackClient().getEventBus();

            eventBus.register(UserChatEvent.class, chatListener);
            eventBus.register(UserJoinEvent.class, joinListener);
            eventBus.register(UserLeaveEvent.class, leaveListener);
        } catch (ServiceOfflineException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unregisters the event listeners from the Dubtrack bot.
     */
    public void unregisterListeners() {
        try {
            EventBus eventBus = SynergyCore.getDubtrackClient().getEventBus();

            eventBus.unregister(chatListener);
            eventBus.unregister(joinListener);
            eventBus.unregister(leaveListener);
        } catch (ServiceOfflineException e) {
            e.printStackTrace();
        }
    }
}
