package net.synergyserver.synergycore.events;

import org.bukkit.event.HandlerList;

import java.util.UUID;

/**
 * This event is called whenever someone clears chat.
 */
public class ChatClearEvent extends ChatEvent {

    private UUID clearer;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>ChatClearEvent</code> with the given parameters.
     *
     * @param clearer The UUID of the person who cleared chat.
     */
    public ChatClearEvent(UUID clearer) {
        super();
        this.clearer = clearer;
    }

    /**
     * Gets the UUID of the player who initiated this event.
     *
     * @return The UUID of the player who initiated this event.
     */
    public UUID getClearer() {
        return clearer;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}