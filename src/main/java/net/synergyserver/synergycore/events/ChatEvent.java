package net.synergyserver.synergycore.events;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;

/**
 * Represents an event that involves chat.
 */
public abstract class ChatEvent extends Event implements Cancellable {

    private boolean cancelled;

    public ChatEvent() {
        this.cancelled = false;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
