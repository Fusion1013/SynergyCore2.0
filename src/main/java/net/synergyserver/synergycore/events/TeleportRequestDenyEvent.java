package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.Teleport;
import org.bukkit.event.HandlerList;

/**
 * This event is called when a player denies a pending <code>Teleport</code>.
 */
public class TeleportRequestDenyEvent extends TeleportRequestEvent {

    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>TeleportRequestDenyEvent</code> with the given <code>Teleport</code>.
     *
     * @param teleport The <code>Teleport</code> of this event.
     */
    public TeleportRequestDenyEvent(Teleport teleport) {
        super(teleport);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}